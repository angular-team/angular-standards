# Testing


Automated testing is an inalienable part of each application. It helps you find bugs in the early stages of software development, reducing expenses and working hours to fix these problems as well.


## Unit tests:
Look at the Test Pyramid. 

![piramid](https://automationpanda.files.wordpress.com/2017/10/the-testing-pyramid.png?w=620)

As you can see, the application should have most of Unit tests.
These tests work directly on the source code and have a full view of everything. The Unit tests work much faster than the e2e, so we can get the picture of the project very fast.

All **business logic, frequently changing functionality** should be covered by Unit tests. 

The number of test coverage should aim at **75-80%**.

**Do not try to achieve 100% total code coverage.** Achieving 100% code coverage sounds good in theory but almost always is a waste of time: you have wasted a lot of effort as getting from 80% to 100% is much more difficult than getting from 0% to 20%.


## E2E / Integration tests:

All **important** flows, like **user registration, login and reset password | adding, updating, removing items | payments**, should be covered by e2e tests.

No need to cover everything, especially some libraries functionality. **E2E tests are very slow**, so you need to decide very carefully what functionality should be covered.

The number of e2e tests should be lesser than the Unit ones.


## Adding test coverage for fixed bugs:
Sometimes we are again facing the bug in production, that has been fixed a few releases ago. This behavior can be prevented: 

If users raise a **critical issue**, you should **fix it and write test cases** for this scenario.


## More info:
Great article about testing anti-patterns and best practices:
http://blog.codepipes.com/testing/software-testing-antipatterns.html
