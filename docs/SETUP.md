# Project Setup

## README.md
Project repository must have **README.md** file, which describes:

- **The name** of the project
- **Badges** from: CI, Code Climate / Sonar Cloud, NPM and etc.
- **Concise description** - What is the goal of a project? Who do use this product? 
- Technologies stack - List of used technologies
- How to set up the local environment
- How to run project locally
- Link to the **WIKI** with detailed documentation


## Editor Config
Every project must have **.editorconfig** file with indents configuration:

```
# Editor configuration, see http://editorconfig.org
root = true
[*]
charset = utf-8
indent_style = space
indent_size = 2
insert_final_newline = true
trim_trailing_whitespace = true
[*.md]
max_line_length = off
trim_trailing_whitespace = false
[*.scss]
indent_size = 4
```


## Proxy Settings (optional)

Create file proxy.config.json

```
  {
    "/api/*": {
      "target": "https://development.server.com",
      "secure": false,
      "logLevel": "debug",
      "changeOrigin": true
    } 
  }
```
Update start script in your package.json
 
 ```
  {
    "scripts": {
      "start": "ng serve --proxy-config proxy.config.json",
    } 
  }
```
