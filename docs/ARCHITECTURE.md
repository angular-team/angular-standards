# Architecture

## File structure

The application should be broken down into the modules. 
All related routes, components, services, models should be in the modules folder. 

Common objects should be located in the shared folder and be the part of the shared module.

#### Here is an example of the project structure:

_It's not a strict rule or finished structure. It depends on a project._


```
  |-- app
      |-- pages
          |-- account
              |-- login
              |-- register
              |-- settings
              |-- password-reset
          |-- home
              |-- home-news
              |-- home-statistics 
              |-- ...
      |-- core
          |-- store (reducers, actions...) 
          |-- interceptors
          |-- layouts
              |-- main
              |-- auth
              |-- error
          |-- utils
          |-- ...
      |-- shared
          |-- components
          |-- services 
          |-- constants 
          |-- directives 
          |-- pipes
          |-- validators
          |-- ...
  
```

Example: https://bitbucket.org/angular-team/angular-standards/src/master/sample-app/


## State management
https://ngrx.io/

It's reasonable to have some state management if the application is going to be huge.

**NgRx** is a framework for building reactive applications in Angular. NgRx provides state management, isolation of side effects, entity collection management, router bindings, code generation, and developer tools that enhance developers experience when building many different types of applications.

It's a good choice for enterprise-level applications.


## Lazy Loading
[How To implement Lazy Loading](https://dev.to/saigowthamr/how-to-implement-lazy--loading-in-angular-6-3ple)

All logic part of the application should be divided into the modules. So we will have the ability to lazy load them. 

It's essential for enterprise-level applications, cause the codebase is significant and the bootstrap time is high. 

Lazy loading allows reducing the bundle size and the bootstrap time to few seconds.


## Hot Module Replacement
[How to enable HMR](https://github.com/angular/angular-cli/wiki/stories-configure-hmr)

This technology updates changed the code on the fly without page refreshing. 

The development will be faster with this technology.


## Resolvers
**Do not use Resolvers.**

For PWA perspectives it's better to show page immediately, and show the loaders for each component, instead of waiting for all requests to be completed. 

So a user will have better experience.


## PWA (Progressive Web App)
Enable PWA into the app. It gives us a lot of benefits:

* Cache bundle and some of the responses.
* PWA allows users to use the site in offline mode. 
* PWA apps are preferable for Search engines. 
* Ability to install the app from a browser on devices.

Also, the App should have at least 80/100 in the [Lighthouse](https://developers.google.com/web/tools/lighthouse/). 
   
   
## Jest test runner
**Do not use Karma.**

Karma is very slow. 

Jest has a bigger set of features (toMatchSnapshot, multiple threads).
