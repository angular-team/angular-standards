# Code style


## Angular style guide

We are using the official style guide from the Angular team: https://angular.io/guide/styleguide


## JS / TS
Please follow this coding style: https://github.com/airbnb/javascript 


## In addition:

### After imports should be 2 blank lines:

```
import { Component } from '@angular/core';


@Component(
```
  
### ES6 imports should be spaced:

```
// bad
import {Component} from '@angular/core';


// good
import { Component } from '@angular/core';
```

### Imports should be separated into three groups:
The first group consists of `@angular/...` imports

Second group consists of `rxjs` imports + third-party libs

The third group consists of your app's src files

```
// bad
import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { Component, OnInit } from '@angular/core';
import { first, takeUntil } from 'rxjs/operators';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MySuperModel } from '<somepath or @app>/models/my-super-model.model.ts';


// good
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalRef, BsModalService } from 'ngx-bootstrap';
import { first, takeUntil } from 'rxjs/operators';

import { MySuperModel } from '<somepath or @app>/models/my-super.model.ts';
```

## TSLint

TSLint should be enabled in each project. 
We are using the default configs from the Angular CLI and a set of rules from [Codelyzer](https://github.com/mgechev/codelyzer).

Before creating PR, make sure new code doesn't have any TSLint errors and warnings.

```
npm run lint
```


## Performance / Best Practices checklist
Make sure the application is matching all points from this checklist:

1. https://github.com/mgechev/angular-performance-checklist 
2. https://medium.com/@spp020/44-quick-tips-to-fine-tune-angular-performance-9f5768f5d945


## Angular Best practices / Advanced functionality

1. https://blog.mgechev.com/post/
2. https://blog.angularindepth.com/
