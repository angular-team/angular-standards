# GIT Style Guide

## Commit message:
Your commits should be **atomic**: https://frederickvanbrabant.com/2017/12/07/atomic-commits.html and commit messages should be **descriptive**.

#### Develop branch flow (all commits are going to develop branch)
At the beginning of the commit message, you should specify the **Jira task numbe**r: PROJECT-123.

```
PROJECT-123: Added header in the currency widget
PROJECT-123: Fixed colors on the performance char
```

#### Feature branch flow (Every feature is being developed in a separated branch)
In this case, we don't need the task number in the commit message, cause we already have it in the PR/branch name

```
Removed icon from header
Refactored markets component
```


## Branch name:
There are a few conventions for naming branches, that can be considered for the project:

#### Descriptive names:

```
feature/header
issue/sidebar-icon
task/replace-font
```
### Or using the task number from Jira:

```
feature-123
issue-123
task-123
```

## Pull Request name:
It should be a task number and a concise description of what have you done:

```
PROJECT-123: Added header
PROJECT-123: Updated portfolios module
PROJECT-123: Removed title from homepage
```
