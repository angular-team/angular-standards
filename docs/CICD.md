# CI / CD
 
## Steps:

All aspects of testing should be automated:

1. The developer creates a **Pull Request** with the awesome feature.
2. Github marks this PR as mergeable (no merge conflicts) and triggers the CI. 
3. CI runs **Unit** and **e2e** tests and sends a coverage report to **Code Climate / Sonar Cloud**. 
4. CI adds a status to the **PR (Passed)**.
5. Now your PR can be merged to **develop** branch.
6. CI deploys the build to **DEV** environment.
7. Now QA may run **manual testing** if it's needed.


## Tools:

1. Github - https://github.com
2. Continuous Integration - need to choose one
  - Travis CI - https://travis-ci.com/ 
  - Jenkins - https://jenkins.io/
  - Circle CI - https://circleci.com/
3. Code quality tools (automated code reviews, coverage tracker): [Code Climate](https://codeclimate.com) / [Sonar Cloud](https://sonarcloud.io/about)
4. Cloud services: Digital Ocean / AWS / Azure
