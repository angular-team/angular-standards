import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PhonePipe } from './pipes/phone.pipe';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BaseInterceptor } from '@core/interceptors/base.interceptor';


const PIPES = [
  PhonePipe
];

@NgModule({
  declarations: [
    ...PIPES
  ],
  imports: [
    CommonModule
  ],
  exports: [
    ...PIPES
  ]
})
export class SharedModule {
  static forRoot() {
    return {
      ngModule: SharedModule,
      providers: [
      ],
    };
  }
}
