# Angular Standards

Angular standards for enterprise-level applications.

### Table of content

| Section | README |
| ------ | ------ |
| Project setup | [docs/SETUP.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/SETUP.md)  |
| Architecture | [docs/ARCHITECTURE.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/ARCHITECTURE.md)  |
| Code Style | [docs/CODE_STYLE.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/CODE_STYLE.md)  |
| GIT Style Guide | [docs/GIT.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/GIT.md)  |
| Testing | [docs/TESTS.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/TESTS.md)  |
| CI / CD | [docs/CICD.md](https://bitbucket.org/angular-team/angular-standards/src/master/docs/CICD.md)  |

 
